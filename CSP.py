""" Truchet Malo et Velland Corentin, Projet ISN 2019, ### Coach Sportif Personnel ### """

#Importation des bibliotheques
from tkinter import * 
import os

""" Création des variable globale et des listes. """

photo = None
identifiant_rectangle = 0
identifiant_rectangle_selectionne = []
fenetre = []

identifiant_rectangle_fenetres = [[[17],[19],[26,28,27],[22],[18],[23,24,25],[20,21]],[[38],[40],[42]],[[55],[57],[59],[61]],[[68],[70],[72],[74]],[[96],[98],[100],[102]]] #Identifiant des rectangles par fenetre
#----------------------------------------------------------------------------------------------------------------------------------

                                                    ### Definition des fenetres ###

""" Les fenetres contiennent des variables qui font appel à une fonction pour etre défini dans le canvas, ce sont soit des
     rectangles soit du texte (les rectangle sont des boutons). Chaque fenetre est d'abord prepare avec une suppression de tout ce qui
                    se trouvait sur le canvas precedent puis il y a l'affichage du rectangle de contour. """

def fenetre_1():

    """ Fenetre 1: Presentation du programme avec un acces aux resultats ou au programme. """
    
    global numero_fenetre

    #Preparation de la fenetre
    can.delete(ALL)
    can.create_rectangle(800, 485, 40, 40, outline='gray95')

    #Texte
    boutongraphique(0, 0, 0, 0, "Bienvenue sur notre programme d'entrainement sportif!", 420, 75, "gray85","", "Arial 25 bold")
    boutongraphique(0, 0, 0, 0, "Cet entrainement sportif vous permettra de suivre des exercices selon \n        votre niveau en accord avec le matériel que vous possedez! \n             Vous trouverez aussi un suivi au fil des séances avec \n                   avec un accès à vos dernières performances", 420, 170, "black","", "Arial 20")
    boutongraphique(0, 0, 0, 0, "C'est le moment de commencer!", 420, 280, "gray85", "", "Arial 23")
    boutongraphique(0, 0, 0, 0, "C'est parti!", 420, 360, "gray85", "", "Arial 25")
    boutongraphique(0, 0, 0, 0, "Vous pouvez aussi consulter vos résultats", 420, 430, "gray85", "", "Arial 25")

    #Rectangle
    boutongraphique(330, 390, 510, 330, "", 0, 0, "", "gray95", "Arial 25")
    boutongraphique(555, 450, 655, 410, "", 0, 0, "", "gray95", "Arial 25")
    
    numero_fenetre = 1
    
def fenetre_2():

    """ Fenetre 2: Création d'un canvas qui affiche une image de corps humain sur lequel il est possible de sélectionner des muscles. """
    
    global photo, numero_fenetre

    #Preparation de la fenetre
    can.delete(ALL)
    can.create_rectangle(800, 485, 40, 40, outline='gray95')

    #Definition du canvas et de l'image    
    photo = PhotoImage(file="corps.jpg")
    can.create_image(120,80, anchor = NW, image=photo)
    can.pack()

    #Liste contenant les coordonnees des rectangles
    coordonnees_rectangles =[(210, 295, 620, 468), 
    (515, 105, 605, 245),
    (515, 245, 605, 295),
    (190, 125, 515, 165),
    (605, 125, 635, 165),
    (240, 185, 298, 280),
    (605, 165, 640, 215),
    (310, 165, 515, 215),
    (190, 165, 225, 220),
    (310, 215, 515, 280),
    (160, 222, 198, 280),
    (630, 215, 670, 280) ]

    #Cree les rectangles pour chaque coordonnees de la liste
    for coordonnees in coordonnees_rectangles:
        fenetre.append(can.create_rectangle(coordonnees))

    #Texte
    boutongraphique(0, 0, 0, 0, "Quel muscle voulez-vous travailler ?", 430, 90, "gray85","", "Arial 25")

    numero_fenetre = 2

def fenetre_3():

    """ Fenetre 3: Choix de la difficulté, Facile/Intermediaire/Difficile. """
    
    global numero_fenetre

    #Preparation de la fenetre
    can.delete(ALL)
    can.create_rectangle(800, 485, 40, 40, outline='gray95')
    
    #Texte
    boutongraphique(0, 0, 0, 0, "Quelle difficulté souhaitez-vous ?", 430, 170, "gray85","", "Arial 25")
    boutongraphique(0, 0, 0, 0, "Facile", 165, 320, "black","", "Arial 24")    
    boutongraphique(0, 0, 0, 0, "Intermediaire", 417, 320, "black","", "Arial 24")
    boutongraphique(0, 0, 0, 0, "Difficile", 675, 320, "black","", "Arial 24")

    #Rectangle
    boutongraphique(85, 290, 245, 350, "", 0, 0, "","gray95", "Arial 25") 
    boutongraphique(315, 290, 515, 350, "", 0, 0, "","gray95", "Arial 25")
    boutongraphique(585, 290, 765, 350, "", 0, 0, "","gray95", "Arial 25")
    
    numero_fenetre = 3

def fenetre_4():

    """ Fenetre 4: Conseil du mode de travail, Explosivite/Puissance/Volume/Andurance. """
    
    global numero_fenetre

    #Preparation de la fenetre
    can.delete(ALL)
    can.create_rectangle(800, 485, 40, 40, outline='gray95')
    
    #Texte
    boutongraphique(0, 0, 0, 0, "Choisissez comment travailler vos muscles", 430, 170, "gray85","", "Arial 25")
    boutongraphique(0, 0, 0, 0, "Explosivité / Puissance: Réalisez l'exercice en utilisant toute l'énergie que vous \n  possédez à chaque répétitions en faisant attention à ne pas sacrifier la forme.", 430, 240, "black","", "Arial 18")    
    boutongraphique(0, 0, 0, 0, "Endurance : Focalisez vous sur votre geste et continuez \n       jusqu'à ce que votre technique se détériore.", 420, 300, "black","", "Arial 18")
    boutongraphique(0, 0, 0, 0, "Volume : Effectuez le geste lentement en vous focalisant sur \n    la contraction des muscles pour obtenir une hypertrophie.", 420, 360, "black","", "Arial 18")
    boutongraphique(0, 0, 0, 0, "Compris !", 417, 450, "black","", "Arial 24")

    #Rectangle
    boutongraphique(315, 430, 515, 470, "", 0, 0, "","gray95", "Arial 25")

    numero_fenetre = 4

def fenetre_5():

    """ Fenetre 5: Choix du materiel, Haltere/Barres parralleles/Barre de traction/Aucun. """
    
    global numero_fenetre

    #Preparation de la fenetre
    can.delete(ALL)
    can.create_rectangle(800, 485, 40, 40, outline='gray95')
    
    #Texte
    boutongraphique(0, 0, 0, 0, "Quel matériel possédez-vous ?", 430, 170, "gray85","", "Arial 25")
    boutongraphique(0, 0, 0, 0, "Haltères", 165, 320, "black","", "Arial 24")    
    boutongraphique(0, 0, 0, 0, "Barres parrallèles", 417, 320, "black","", "Arial 18")
    boutongraphique(0, 0, 0, 0, "Barre de traction", 675, 320, "black","", "Arial 18")
    boutongraphique(0, 0, 0, 0, "Aucun", 417, 420, "black","", "Arial 24")
    
    #Rectangle
    boutongraphique(85, 290, 245, 350, "", 0, 0, "","gray95", "Arial 25") 
    boutongraphique(315, 290, 515, 350, "", 0, 0, "","gray95", "Arial 25")
    boutongraphique(585, 290, 765, 350, "", 0, 0, "","gray95", "Arial 25")
    boutongraphique(315, 390, 515, 450, "", 0, 0, "","gray95", "Arial 25")

    numero_fenetre = 5



def fenetre_6():

    """ Fenetre 6: Affichage des exercices en fonction des parametres rentres dans les fenetres precedentes.

        --> entré: Identifiant des rectangles sélectionnés pour la difficulté, le muscle, les matériaux.
        <-- sortie: Affichage des exercuces qui correspondent aux sélections.
    """
    
    global numero_fenetre

    #Preparation de la fenetre
    can.delete(ALL)
    can.create_rectangle(800, 485, 40, 40, outline='gray95')

    #Variable locale
    verification_nombre_exercice = []
    espace_entre_exercices = 80
    
    #Tester si le fichier existe et affichage d'un message d'erreur s'il n'existe pas.
    if not os.path.exists("exercices.txt"):
        boutongraphique(0, 0, 0, 0, "Le fichier d'exercice n'existe pas", 430, 170, "gray85","", "Arial 21")
        boutongraphique(0, 0, 0, 0, "fermez le programme et placez le fichier dans votre dossier", 430, 200, "gray85","", "Arial 21")


    #Lecture et traitement du fichier en fonction des choix de l'utilisateur.      
    with open('exercices.txt','r', encoding='utf-8') as fichier_exercice:
        for exercices in fichier_exercice:
            
            if int(exercices[0]) == int(identifiant_rectangle_selectionne[0]) and int(exercices[2]) == int(identifiant_rectangle_selectionne[3]): #Test si les exercices dans le fichier correspondent a la demande de l'utilisateur
                verification_nombre_exercice.append(exercices)
                espace_entre_exercices += 80
                exercices_decoupe = exercices.split('_')
                boutongraphique(0, 0, 0, 0, exercices_decoupe[1].replace('-',' ') + "--> " + "Répétition : " + "   " + str(int(exercices[1])*int(identifiant_rectangle_selectionne[1]+1)), 430, espace_entre_exercices, "gray85","", "Arial 23")
                
        if len(verification_nombre_exercice) == 0:
            #Texte affichage d'un message d'erreur s'il n'existe pas d'exercice en fonction de la demande de l'utilisateur
            boutongraphique(0, 0, 0, 0,"Il n'existe pas d'exercice de ce type", 430, espace_entre_exercices, "gray85","", "Arial 23")
            sys.exit()
            
        
    #Texte
    boutongraphique(0, 0, 0, 0, "Terminé", 420, 430, "gray85", "", "Arial 25")
    boutongraphique(0, 0, 0, 0, "Exercice à réaliser :", 430, 80, "gray85", "", "Arial 25")

    #Rectangle
    boutongraphique(330, 460, 510, 400, "", 0, 0, "", "gray95", "Arial 25")
    
    numero_fenetre = 6

def fenetre_7():

    """ Fenetre 7: Demande du nombre de répétitions réalisées. """
        
    global numero_fenetre

    #Preparation de la fenetre
    can.delete(ALL)
    can.create_rectangle(800, 485, 40, 40, outline='gray95')
    
    #Texte
    boutongraphique(0, 0, 0, 0, "Avez-vous fait toutes les répétitions ?", 430, 170, "gray85","", "Arial 25")
    boutongraphique(0, 0, 0, 0, "Moins de la moitier", 165, 320, "black","", "Arial 18")    
    boutongraphique(0, 0, 0, 0, "Toutes", 417, 320, "black","", "Arial 24")
    boutongraphique(0, 0, 0, 0, "Plus de la moitier", 675, 320, "black","", "Arial 18")
    boutongraphique(0, 0, 0, 0, "Aucune", 417, 420, "black","", "Arial 24")
    
    #Rectangle
    boutongraphique(85, 290, 245, 350, "", 0, 0, "","gray95", "Arial 25") 
    boutongraphique(315, 290, 515, 350, "", 0, 0, "","gray95", "Arial 25")
    boutongraphique(585, 290, 765, 350, "", 0, 0, "","gray95", "Arial 25")
    boutongraphique(315, 390, 515, 450, "", 0, 0, "","gray95", "Arial 25")

    numero_fenetre = 7
    

def calculer_score():

    """ Calcul un score en fonction des repetitions qu'à realisé l'utilisateur.

        --> entrée: Identifiant du rectangle sélectionné pour la difficulté.
        <-- sortie: Score de l'utilisateur.
    """
    
    if int(identifiant_rectangle_selectionne[4]) == 3: #Aucune : score = 0
        score_utilisateur = 0
    elif int(identifiant_rectangle_selectionne[4]) == 1: #Toutes : score = 10
        score_utilisateur = 10
    else: #Moins de la moitier : score = 2.5 / Plus de la moitier : score = 7.5
        score_utilisateur = (int(identifiant_rectangle_selectionne[4]+1))*(2.5) 
    return score_utilisateur


def enregistrer_progression():

    """ Enregistre la progression de l'utilisateur dans un fichier texte 'resultats'.

        --> entrée: Nom sous lequel l'utilisateur veut etre enregistré et son score.
        <-- sortie: Enregistrement du score de l'utilisateur à la suite de son nom.
    """
    
    global nom_renseigne

    #Variable locale
    nom_utilisateur = nom_renseigne.get()
    verification_utilisateur_existant = []
    score = str(calculer_score())+"/10"

    #Tester si le fichier existe et affichage d'un message d'erreur s'il n'existe pas
    if not os.path.exists("resultats.txt"):
        can.delete(ALL)
        can.create_rectangle(800, 485, 40, 40, outline='gray95')
        boutongraphique(0, 0, 0, 0, "Le fichier de résultats n'existe pas.", 430, 170, "gray85","", "Arial 21")

    #Lecture du fichier et stockage dans la variable 'tableur_ligne_resultat' sous forme de tableur.
    with open('resultats.txt', 'r', encoding='utf-8') as fichier_resultat:
        tableur_ligne_resultat=fichier_resultat.readlines()

    #Double lecture avec la ligne et son compteur pour intervenir sur la ligne
    for (compteur_ligne, ligne_tableur) in enumerate(tableur_ligne_resultat):
        ligne_tableur=ligne_tableur.replace("\n", "")
        
        if ligne_tableur.startswith(nom_utilisateur): #Si la ligne du tableur commence par le nom de l'utilisateur alors on ajoute le score à la suite de la ligne 
            tableur_ligne_resultat[compteur_ligne]="%s_%s\n" % (ligne_tableur, score)
            verification_utilisateur_existant.append(nom_utilisateur)
            
    if len(verification_utilisateur_existant) == 0: #Si le nom de l'utilisateur n'existe pas dans le fichier alors on ajoute l'utilisateur avec son nouveau score
        tableur_ligne_resultat.append(nom_utilisateur+'_'+score)
        
    #Reecriture de la variable 'tableur_ligne_resultat' modifie en entier dans le fichier
    with open('resultats.txt', 'w', encoding='utf-8') as fichier_resultat:
        fichier_resultat.writelines(tableur_ligne_resultat)

    #Destruction de la fenetre
    fen.destroy()

def fenetre_8():

    """ Fenetre 8: Demande si le score doit être enregistre au nom de l'utilisateur ou non. """
        
    global numero_fenetre, nom_renseigne

    #Preparation de la fenetre
    can.delete(ALL)
    can.create_rectangle(800, 485, 40, 40, outline='gray95')
    
    #Texte
    boutongraphique(0, 0, 0, 0, "Voulez-vous enregistrer votre progression ?", 430, 170, "gray85","", "Arial 25")
    boutongraphique(0, 0, 0, 0, "Ne pas enregistrer", 435, 350, "black","", "Arial 20")
    boutongraphique(0, 0, 0, 0, "Nom : ", 170, 250, "gray95","", "Arial 20")

    #Rectangle
    boutongraphique(320, 320, 550, 380, "", 0, 0, "","gray95", "Arial 25")
    
    #Entree de texte
    nom_renseigne = Entry(can)
    can.create_window(410, 250, window=nom_renseigne, height=40, width=400)

    #Bouton
    bouton_nom_renseigne = Button(text='Enregistrer', command=enregistrer_progression)
    can.create_window(670, 251, window=bouton_nom_renseigne, height=40, width=100)
 
    numero_fenetre = 8

def fenetre_resultat():

    """ Demande du nom de l'utilisateur recherche.
    
       --> sortie: nom recherché et rentré par l'utiliateur dans un widget Entry
    """
    
    global nom_entre, numero_fenetre

    #Preparation de la fenetre
    can.delete(ALL)
    can.create_rectangle(800, 485, 40, 40, outline='gray95')

    #Texte
    boutongraphique(0, 0, 0, 0, "Nom recherché : ", 200, 250, "gray95","", "Arial 20")

    #Entree de texte
    nom_entre = Entry(can)
    can.create_window(530, 250, window=nom_entre, height=40, width=400)

    #Bouton
    bouton_nom_entre = Button(text='OK', command=fenetre_recherche)
    can.create_window(750, 250, window=bouton_nom_entre, height=40, width=40)

    numero_fenetre = 9

def fenetre_recherche():

    """ Recherche de l'utilisateur dans le fichier 'resultats', affiche ses résultats si il est trouvé
              dans le fichier, seulement les 5 derniers résultats de l'utilisateur son affichés.

        --> entrée: Nom recherché par l'utilisateur sous forme de chaine de caractère (via widget Entry)
        <-- sortie: Affichage des 5 derniers résultats de l'utilisateur recherché, si celui-ci existe, sinon un message d'erreur apparait.
    """

    global nom_entre

    #Variable locale
    nom_recherche = nom_entre.get()
    espace_entre_resultat = 140
    verification_nom_existant = []

    #Preparation de la fenetre
    can.delete(ALL)
    can.create_rectangle(800, 485, 40, 40, outline='gray95')

    #Tester si le fichier existe et affichage d'un message d'erreur s'il n'existe pas
    if not os.path.exists("resultats.txt"):
        boutongraphique(0, 0, 0, 0, "Le fichier de résultats n'existe pas", 430, 170, "gray85","", "Arial 21")

    #Lecture du fichier et traitement en fonction du nom recherche.        
    with open('resultats.txt','r', encoding='utf-8') as resultat_fichier:
        for resultats in resultat_fichier:
            resultat_decoupe = resultats.split('_')
            
            if resultat_decoupe[0] == nom_recherche: #Test si le nom existe dans le fichier
                verification_nom_existant.append(resultat_decoupe[0])
                nombre_resultat=len(resultat_decoupe)
                nombre_resultat_afficher = nombre_resultat - 5
                boutongraphique(0, 0, 0, 0, resultat_decoupe[0], 430, espace_entre_resultat, "gray85","", "Arial 23") #Affichage du nom recherche
                
                if nombre_resultat > 5: #Test si il y existe plus de 5 resultats (5 score affiche maximum) et prend les 5 derniers
                    for i in range(nombre_resultat_afficher, nombre_resultat):
                        espace_entre_resultat += 50
                        boutongraphique(0, 0, 0, 0, resultat_decoupe[i], 430, espace_entre_resultat, "gray85","", "Arial 23")
                        
                else: #Il y a moins de 5 resultats et ils sont tous affiche
                    for i in range(1,nombre_resultat):
                        espace_entre_resultat += 50
                        boutongraphique(0, 0, 0, 0, resultat_decoupe[i], 430, espace_entre_resultat, "gray85","", "Arial 23")
                        
        if len(verification_nom_existant)== 0: #Si le nom existe pas dans le fichier, affichage message d'erreur
            boutongraphique(0, 0, 0, 0, "Ce nom n'existe pas.", 430, espace_entre_resultat, "gray85","", "Arial 23")

#----------------------------------------------------------------------------------------------------------------------------------

                                              ### Definition des fonction utiles au programme ###
def parametre_valeur_identifiant_rectangle(grande_liste):

    """ Reconnaissance des rectangle sur lesquels l'utilisateur a cliqué, a partir
                 d'une liste contenat les identifiants de chaque rectangle

        --> entrée: Une liste (arbre), contenant tout les identifiants des rectangles par fenetre.
        <-- sortie: Index de l'identifiant du rectangle sur lequel l'utilisateur à cliqué.
    """
    
    for liste in grande_liste:
        for petite_liste in liste:
            for valeurs_rectangle in petite_liste:
                
                if identifiant_rectangle == valeurs_rectangle: #Test si l'identifiant du rectangle dans la liste vaux celui selectionne par l'utilisateur, son index devient une valeur de parametre
                    valeur_parametre_rectangle=liste.index(petite_liste)
                    identifiant_rectangle_selectionne.append(valeur_parametre_rectangle)
            
def boutongraphique(unX0, unY0, unX1, unY1, unTexte, unXt, unYt, uneCouleur, unContour, unePolice):

    """ Creation de texte et rectangle dans le canvas pour chaque fenetre.

        --> entrée: Paramètres pour créer un rectangle(nX0, unY0, unX1, unY1, unContour)
                    ou une zone de texte (unTexte, unXt, unYt, uneCouleur, unePolice).
        <-- sortie: Affichange des rectangle et/ou zone de texte si la fonction est appelée.
    """
    
    coordonnees_rectangle = can.create_rectangle(unX0, unY0, unX1, unY1, outline=unContour)
    fenetre.append(coordonnees_rectangle)
    can.create_text(unXt, unYt, text=unTexte, font=unePolice, fill=uneCouleur)

def clique_gauche(event):

    """ Le clique gauche est associé a cette fonction qui, lorsque le clique est
              effectué entre les coordonnées d'un rectangle, il y a une
                           réponse specifique.

        --> entrée: Coordonées de la position de l'évenement 'clique gauche'.
        <-- sortie: Affiche une fenetre en fonction de l'endoit du clique.
    """

    global numero_fenetre, identifiant_rectangle

    #Variable locale
    X = event.x   #Position du pointeur de la souris
    Y = event.y

    for rectangle in fenetre: #Fenetre contient les coordonnees de chaque rectangle pour chacune des fenetre
        [x1,y1,x2,y2] = can.coords(rectangle)

        
        if x1<=X<=x2 and y1<=Y<=y2 : #Test pour chaque rectangle si la position du clique est realise entre les coordonnees d'un rectangle
            identifiant_rectangle=rectangle
            parametre_valeur_identifiant_rectangle(identifiant_rectangle_fenetres)
            
            
            if identifiant_rectangle == 14: #Si le rectangle possede l'identifiant 14 alors la fenetre de resultat s'ouvre
                fenetre[:]= [] #Vider la liste
                fenetre_resultat()
                
            elif identifiant_rectangle == 21 and numero_fenetre == 9: #Si le rectangle possede l'identifiant 21 et il est positionne dans la fenetre 9 alors la fenetre de recherche s'ouvre
                fenetre[:]= []
                fenetre_recherche()
                
            elif identifiant_rectangle == 111 and numero_fenetre == 8: #Si le rectangle possede l'identifiant 115 et il est positionne dans la fenetre 8 alors la fenetre se ferme
                fen.destroy()
                
            else: #Sinon c'est la fenetre suivante (nombre croissant) qui s'ouvre
                fenetre[:]= [] 
                fenetre_ordre_croissant[numero_fenetre]()
        
                
#----------------------------------------------------------------------------------------------------------------------------------
                                                ### Definition principale du programme ###
            
""" Definition de la fenetre et de ses caractéristiques adaptatives. """
        
fen=Tk()

fen.title("Entrainement Sportif")

""" Définition de la taille de la fenêtre et son placement, définition du canvas dans la fenetre créé. """

screen_width= fen.winfo_screenwidth() #Obtention taille écran (largeur)
screen_height= fen.winfo_screenheight() #Obtention taille écran (hauteur)
screen_height= int(screen_height/2)
screen_width= int(screen_width/2)
x= int((screen_width)-(screen_width/2))
y= int((screen_height)-(screen_height/2))
fen.geometry("{}x{}+{}+{}".format(screen_width,screen_height,x,y)) #Mise en page de la fenetre au centre de l'écran et de la taille d'un quart de l'écran

can = Canvas(fen, width=screen_width,height= screen_height,background='gray40') #Creation d'un canvas
can.pack()
can.bind('<1>', clique_gauche) #Association du clique gauche à la fonction 'clique_gauche'
fenetre_1()
fenetre_ordre_croissant =[fenetre_1, fenetre_2, fenetre_3, fenetre_4, fenetre_5, fenetre_6, fenetre_7, fenetre_8]


##mainloop
fen.mainloop()
